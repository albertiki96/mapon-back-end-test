
# Mapon back-end test

## Project initialization 🚀

_These instructions will allow you to install the project on your local machine._

 1. Extract the project on your **local server**.
 2. Run the SQL script mapon.sql **on your local server (127.0.0.1)**. This will create a database called "**mapon**" in which you will have a table called "**users**". Inside this table there will be a user created. (User: albertoabenzaromero@gmail.com Password: 123456).
 3. In a browser, access the project using the following [URL](http://localhost/mapon-back-end-test/): http://localhost/mapon-back-end-test/
 4. The project is initialized successfully. Access through the user **albertoabenzaromero@gmail.com** and the password **123456** to consult the routes.

  

## Description of the architecture 🔧

_This project consists essentially of 3 PHP files and 1 JavaScript file._

### PHP Files:

 1. *index.php:* This file is the first that will appear to us when accessing our project. It contains the login form.
 2. *login.php:* This file will be called when trying to log in from the previous form. This will be in charge of checking if the user that we have entered exists in our database.
	- If it exists, it will redirect us to our map.php file.
	- If it does not exist, it will redirect us to the index.php file, the form will be painted red and an error message will be displayed.
 3. *map.php:* This file will show us the map in which the routes will be loaded and a form that will allow us to choose different time periods (date from / to). We can choose if we want to make a normal request or an AJAX request. When the request is normal, this file will also be in charge of calling the Mapon API with the data entered.
	- If the data entered is wrong, this will also provide the error message.
	- If the data is correct, it will load the routes and stops of the car on the map.

  ### JavaScript File:
  

 1. *scripts.js:* This file will essentially be in charge of loading the map with or without data and making the AJAX request.


## What makes my test special?

 - :fast_forward: I have used minified files to accelerate the loading speed of the application.
 - :cherry_blossom: I use lotties animations and fonts to make the app look prettier.
 - :envelope: I use AJAX so that the user does not experience sudden jumps on the web.
 - :vibration_mode: The whole project is responsive. It is perfectly seen from a mobile.
---

⌨️ by [Alberto Abenza Romero](https://curriculumalbertoabenza.com) 😊