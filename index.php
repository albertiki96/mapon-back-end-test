<?php
//Errors control
$error = 0;

if (isset($_GET['error'])) {
    $error = $_GET['error'];
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mapon</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
    <link href="css/style.css" rel="stylesheet">
    <script>
        var base_url = "<?php echo $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/mapon-back-end-test/'; ?>";
    </script>
</head>

<body>
    <div class="background">
        <div class="container">
            <div class="row absolute-center viewport-height">
                <form class="login col-lg-4 col-md-6 col-sm-12" action="login.php" method="post">
                    <h1>Login to Mapon</h1>
                    <div class="mb-4">
                        <label for="inputEmail1" class="form-label <?php if ($error == 1) {
                                                                        echo 'is-invalid';
                                                                    } ?>">Email address</label>
                        <input type="email" name="email" class="form-control <?php if ($error == 1) {
                                                                                    echo 'is-invalid';
                                                                                } ?>" id="inputEmail1">
                    </div>
                    <div class="mb-4">
                        <label for="inputPassword1" class="form-label <?php if ($error == 1) {
                                                                            echo 'is-invalid';
                                                                        } ?>">Password</label>
                        <input type="password" name="password" class="form-control <?php if ($error == 1) {
                                                                                        echo 'is-invalid';
                                                                                    } ?>" id="inputPassword1">
                    </div>
                    <?php if ($error == 1) { ?>
                        <div class="alert alert-danger" role="alert">
                            The credentials are not correct.
                        </div>
                    <?php } ?>
                    <div class="mb-1">
                        <button type="submit" class="btn btn-primary full-width">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/scripts.js"></script>
</body>

</html>