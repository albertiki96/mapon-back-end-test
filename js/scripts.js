// Fade in background image
if ($("div.background").length > 0) {
    var image = new Image();
    image.onload = function () {
        $("div.background").css("background-image", "url('" + image.src + "')");
    }

    image.src = base_url + "images/background.jpg"; //image to be transitioned to
}

function calculateAverageCoordinates(routes) {
    var sumLats = 0;
    var sumLngs = 0;
    var countLats = 0;
    var countLngs = 0;
    var averageLats = 0;
    var averageLngs = 0;

    routes.forEach(function (item, index) {
        sumLats += item.start.lat;
        countLats++;
        sumLngs += item.start.lng;
        countLngs++;

        if (item.type == "route") {
            sumLats += item.end.lat;
            countLats++;
            sumLngs += item.end.lng;
            countLngs++;
        }

        if (index == routes.length - 1) {
            averageLats = sumLats / countLats;
            averageLngs = sumLngs / countLngs;
        }

    });

    return { averageLats: averageLats, averageLngs: averageLngs };
}

function loadMap(response = false) {
    if ($('#map').length > 0) {
        var mapDiv = document.getElementById("map");
        if (response.data) {
            var routes = response.data.units[0].routes;

            averageCoordinates = calculateAverageCoordinates(routes);

            var mapOptions = {
                center: new google.maps.LatLng(averageCoordinates.averageLats, averageCoordinates.averageLngs),
                zoom: 5,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(mapDiv, mapOptions);

            var destinations;
            var polylineOptions;
            var polyline;
            var marker;

            routes.forEach(function (item) {
                if (item.type == "stop") {
                    marker = new google.maps.Marker({
                        position: { lat: item.start.lat, lng: item.start.lng },
                        map: map
                    });
                }
                if (item.type == "route") {
                    destinations = [];
                    destinations.push(new google.maps.LatLng(item.start.lat, item.start.lng));
                    destinations.push(new google.maps.LatLng(item.end.lat, item.end.lng));

                    polylineOptions = { path: destinations, strokeColor: '#ff0000', strokeWeight: 2 };

                    polyline = new google.maps.Polyline(polylineOptions);

                    polyline.setMap(map);
                }
            });
        }
        else {
            var mapOptions = {
                center: new google.maps.LatLng(0, 0),
                zoom: 2,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
    
            var map = new google.maps.Map(mapDiv, mapOptions);
        }
    }
}

function removeErrors() {
    $('.alert-danger').remove();
    $('.is-invalid').removeClass('is-invalid');
}

function detectLastInput() {
    var numOfInputs = $('form > div > input').length;
    return $('form > div > input').parent()[numOfInputs - 1];
}

function isMobile() {
    var isMobile = false; //initiate as false

    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
        isMobile = true;
    }

    return isMobile;
}

$(document).on('click', '.jsFormDatesAjax', function (e) {
    e.preventDefault();

    removeErrors();

    var formValid = true;

    var form = $(this).closest('form');

    var from = form.find('input#inputFrom').val();
    var till = form.find('input#inputTill').val();

    if (from == "") {
        $('label[for=inputFrom]').addClass('is-invalid');
        form.find('input#inputFrom').addClass('is-invalid');
        formValid = false;
    }
    if (till == "") {
        $('label[for=inputTill]').addClass('is-invalid');
        form.find('input#inputTill').addClass('is-invalid');
        formValid = false;
    }

    if (formValid) {
        // We add the last Z manually
        from = from + "Z";
        till = till + "Z";

        var method = 'get';
        var urlUnits = 'https://mapon.com/api/v1/unit/list.json';
        var urlRoutes = 'https://mapon.com/api/v1/route/list.json';
        var dataUnits = { key: '92c294ae1680d5a2bc3f2e64fb5082cb89667606' };

        // We get the units
        $.ajax({
            url: urlUnits,
            type: method,
            data: dataUnits,
            beforeSend: function () {
                $('#map').html('<div class="loading"><div id="lottie" class="lottie"></div></div>');
                var animation = bodymovin.loadAnimation({
                    container: document.getElementById('lottie'),
                    renderer: 'svg',
                    loop: true,
                    autoplay: true,
                    path: base_url + 'animations/loading_map.json'
                });
                animation.setSpeed(2.5);
            },
            success: function (response) {
                var unit_id = response.data.units[0].unit_id;
                var include = ['decoded_route', 'polyline'];
                var dataRoutes = { key: '92c294ae1680d5a2bc3f2e64fb5082cb89667606', from: from, till: till, unit_id: unit_id, include: include[1] };

                // We get the routes
                $.ajax({
                    url: urlRoutes,
                    type: method,
                    data: dataRoutes,
                    success: function (response) {
                        // Error control
                        if (response.error) {
                            // 1 Invalid or unknown unit_id parameter (NOT POSSIBLE)
                            // 6 decoded_route parameter available only when requesting routes for one unit (NOT POSSIBLE)
                            // 7 Invalid or unknown driver_id parameter (NOT POSSIBLE)
                            // 8 Invalid or unknown route_types parameter (NOT POSSIBLE)

                            // 2 Invalid or missing from parameter
                            if (response.error.code == 2) {
                                $('#inputFrom').addClass('is-invalid');
                            }
                            // 3 Invalid or missing till parameter
                            if (response.error.code == 3) {
                                $('#inputTill').addClass('is-invalid');
                            }

                            // 4 Invalid period
                            // 5 Period can not exceed 31 days
                            if (response.error.code == 4 || response.error.code == 5) {
                                $('input[type=datetime-local]').addClass('is-invalid');
                            }

                            $('<div class="alert alert-danger" role="alert">' + response.error.msg + '</div>').insertAfter(detectLastInput());

                            loadMap(response);
                        }
                        else {
                            loadMap(response);

                            if (isMobile()) {
                                // We do scroll to map
                                var $targetEle = $('#map');

                                $('html, body').animate({
                                    'scrollTop': $targetEle.offset().top
                                }, 1);
                            }
                        }
                    },
                });
            },
        });

    }
});


