<?php
// You can only be here if you are logged in.
session_start();
if ((isset($_SESSION['session_active']) && $_SESSION['session_active'] == false) || !isset($_SESSION['session_active'])) {
    header("Status: 301 Moved Permanently");
    header("Location:./index.php");
}


if (count($_POST) > 0) {
    $key = '92c294ae1680d5a2bc3f2e64fb5082cb89667606';

    // We get the car id (unit_id)
    $url = 'https://mapon.com/api/v1/unit/list.json?key=' . $key;

    $cars = file_get_contents($url);
    $cars_json = json_decode($cars);

    $from = $_POST['from'] . 'Z';
    $till = $_POST['till'] . 'Z';

    $unit_id = $cars_json->data->units[0]->unit_id;

    $include = array('decoded_route', 'polyline');


    // We get the routes of the car
    $url = 'https://mapon.com/api/v1/route/list.json?key=' . $key . '&from=' . $from . '&till=' . $till . '&unit_id=' . $unit_id . '&include=' . $include[1];

    $routes = file_get_contents($url);
    $routes_json = json_decode($routes);

}

$error = 0;

if(isset($routes_json->error)){
    $error = $routes_json->error->code;
}




?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mapon</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
    <link href="css/style.css" rel="stylesheet">
    <script>
        var base_url = "<?php echo $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/mapon-back-end-test/'; ?>";
    </script>
</head>

<body>
    <div class="">
        <div class="align-center viewport-height">
            <div class="grid5050">
                <form class="dates" action="" method="post">
                    <h1>Check routes by dates</h1>
                    <div class="mb-3">
                        <label for="inputFrom" class="form-label <?php if($error == 2 || $error == 5 || $error == 6) echo 'is-invalid'; ?>">From</label>
                        <input type="datetime-local" step="1" name="from" class="form-control <?php if(isset($routes_json->error)) echo 'is-invalid'; ?>" id="inputFrom" required>
                    </div>
                    <div class="mb-4">
                        <label for="inputTill" class="form-label <?php if($error == 3 || $error == 5 || $error == 6) echo 'is-invalid'; ?>">Till</label>
                        <input type="datetime-local" step="1" name="till" class="form-control <?php if(isset($routes_json->error)) echo 'is-invalid'; ?>" id="inputTill" required>
                    </div>
                    <?php if($error > 0){ ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $routes_json->error->msg ?>
                        </div>
                    <?php } ?>
                    <button type="submit" class="btn btn-primary full-width mb-3">Check routes</button>
                    <button class="btn btn-primary full-width jsFormDatesAjax">Check routes (AJAX)</button>
                </form>
                <div id="map" class="map"></div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- I could use Mapon's Google Maps API Key (AIzaSyAVsUtb1lHPboW7F9MmrFWdVJfGyAuKuMY) to remove the message "For development purposes only". -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBi4IdH6aV2aoTPxhgE70QUA6tZ3-VZymQ"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.7.8/lottie.min.js" integrity="sha512-w58XJ/0dVbEfZVJoJyBUOTMGBWmIW4jEYJSA0898d2L0Ghpum0FvwK7qTuHhkTctApzrchv3Neli+28ajI4+fg==" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script>
        function init() {
            <?php if (count($_POST) > 0) { ?>
                loadMap(<?php echo $routes; ?>);
            <?php }else{ ?>
                loadMap();
            <?php } ?>
        }
        window.onload = init;
    </script>
</body>

</html>